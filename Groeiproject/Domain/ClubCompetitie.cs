﻿using System.ComponentModel.DataAnnotations;

namespace SC.BL.Domain
{
    public class ClubCompetitie
    {
        [Required] public Club Club { get; set; }
        [Required] public Competitie Competitie { get; set; }
    }
}