﻿using System;
using System.Collections.Generic;
using SC.BL.Domain;

namespace SC.BL
{
    public interface IManager
    {
        public Club GetClub(int id);
        public Club GetClubWithCompetitionsAndPlayers(int id);
        public List<Club> GetAllClubs();
        public List<Club> GetAllClubsWithCompetitions();
        public List<Club> GetClubsOfCountry(string country);
        public Competitie GetClubsOfCompetition(int competitieId);
        public Club AddClub(string naam, string land, int capaciteitStadion, DateTime oprichtingsdatum, String trainer);

        public Competitie GetCompetition(int id);
        public List<Competitie> GetAllCompetitions();
        public List<Competitie> GetAllCompetitionsWithClubs();
        public List<Competitie> GetCompetitionsByNameAndOprichtingsjaar(string name, int oprichtingsjaar);
        public Competitie AddCompetition(string naam, string locatie, int oprichtingsjaar);

        public void AddClubCompetition(ClubCompetitie clubCompetitie);
        public void RemoveClubCompetition(int clubId, int competitieId);

        public Speler GetPlayer(int id);
        public List<Speler> GetAllPlayers();
        public List<Speler> GetAllPlayersWithClub();
    }
}