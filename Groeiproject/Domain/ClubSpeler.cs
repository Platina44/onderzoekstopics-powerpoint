﻿using System.ComponentModel.DataAnnotations;

namespace SC.BL.Domain
{
    public class ClubSpeler
    {
        [Required] public Club Club { get; set; }
        [Required] public Speler Speler { get; set; }
    }
}