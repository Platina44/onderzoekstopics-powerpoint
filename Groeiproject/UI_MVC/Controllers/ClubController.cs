﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using SC.BL;
using SC.BL.Domain;

namespace UI_MVC.Controllers
{
    public class ClubController : Controller
    {
        Manager _manager = new Manager();

        public IActionResult Index()
        {
            return View(_manager.GetAllClubs());
        }

        public IActionResult ClubDetail(int id)
        {
            return View(_manager.GetClubWithCompetitionsAndPlayers(id));
        }

        public IActionResult CompetitieDetail(int id)
        {
            return View(_manager.GetCompetition(id));
        }

        public IActionResult SpelerDetail(int id)
        {
            return View(_manager.GetPlayer(id));
        }

        [HttpGet]
        public IActionResult AddClub()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddClub(Club c)
        {
            Club newClub = _manager.AddClub(c.Naam, c.Land, c.CapaciteitStadion, c.Oprichtingsdatum, c.Trainer);

            if (!ModelState.IsValid)
            {
                return View(c);
            }

            return RedirectToAction("ClubDetail", "Club",
                new {id = newClub.Id});
        }

        [HttpGet]
        public IActionResult Competitions()
        {
            return View(_manager.GetAllCompetitions());
        }

        [HttpPost]
        public IActionResult Competitions(string name, int oprichtingsjaar)
        {
            if (name == null)
            {
                name = "";
            }

            if (oprichtingsjaar == 0)
            {
                oprichtingsjaar = DateTime.Now.Year;
            }

            return View(_manager.GetCompetitionsByNameAndOprichtingsjaar(name, oprichtingsjaar));
        }

        public IActionResult AddRemoveClubCompetition(int competitieId, int clubId, bool addRemove)
        {
            if (addRemove)
            {
                ClubCompetitie cc = new ClubCompetitie
                    {Club = _manager.GetClub(clubId), Competitie = _manager.GetCompetition(competitieId)};
                _manager.AddClubCompetition(cc);
            }
            else
            {
                _manager.RemoveClubCompetition(clubId, competitieId);
            }

            List<Club> otherClubs = new List<Club>();

            foreach (Club club in _manager.GetAllClubs())
            {
                bool ok = true;
                
                foreach (ClubCompetitie cc in _manager.GetClubsOfCompetition(competitieId).ClubsInCompetities)
                {
                    if (club.Id.Equals(cc.Club.Id))
                    {
                        ok = false;
                    }
                }

                if (ok)
                {
                    otherClubs.Add(club);
                }
            }
            
            ViewBag.Competition = _manager.GetCompetition(competitieId);
            ViewBag.ClubsOfCompetition = _manager.GetClubsOfCompetition(competitieId).ClubsInCompetities;
            ViewBag.OtherClubs = otherClubs;

            return View(ViewBag);
        }
    }
}