﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SC.BL.Domain
{
    public class Competitie
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(25, ErrorMessage = "Er zijn maximaal 25 karakters toegelaten in de naam!")]
        public string Naam { get; set; }
        [StringLength(25, ErrorMessage = "Er zijn maximaal 25 karakters toegelaten in de naam!")]
        public string Locatie { get; set; }
        public int Oprichtingsjaar { get; set; }
        public ICollection<ClubCompetitie> ClubsInCompetities { get; set; }

        public Competitie()
        {
            this.Id = 0;
            this.Naam = "Onbekend";
            this.Locatie = "Onbekend";
            this.Oprichtingsjaar = 0;
        }
        
        public Competitie(string naam, string locatie, int oprichtingsjaar)
        {
            this.Naam = naam;
            this.Locatie = locatie;
            this.Oprichtingsjaar = oprichtingsjaar;
        }

        public Competitie(int id, string naam, string locatie, int oprichtingsjaar)
        {
            this.Id = id;
            this.Naam = naam;
            this.Locatie = locatie;
            this.Oprichtingsjaar = oprichtingsjaar;
        }

        public override string ToString()
        {
            return $"{Naam, -27}{Locatie, -21}{Oprichtingsjaar, -7}";
        }
    }
}