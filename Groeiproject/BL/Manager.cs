﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SC.BL.Domain;
using SC.DAL.EF;

namespace SC.BL
{
    public class Manager : IManager
    {
        private readonly Repository _repository;
            //private readonly InMemoryRepository _repository;

        public Manager()
        {
            _repository = new Repository();
            //_repository = new InMemoryRepository();
        }


        public Club GetClub(int id)
        {
            return _repository.ReadClub(id);
        }

        public Club GetClubWithCompetitionsAndPlayers(int id)
        {
            return _repository.ReadClubWithCompetitionAndPlayers(id);
        }

        public List<Club> GetAllClubs()
        {
            return _repository.ReadAllClubs();
        }

        public List<Club> GetAllClubsWithCompetitions()
        {
            return _repository.ReadAllClubsWithCompetitions();
        }

        public List<Club> GetClubsOfCountry(string country)
        {
            return _repository.ReadClubsOfCountry(country);
        }

        public Competitie GetClubsOfCompetition(int competitieId)
        {
            return _repository.ReadClubsOfCompetition(competitieId);
        }

        public Club AddClub(string naam, string land, int capaciteitStadion, DateTime oprichtingsdatum, String trainer)
        {
            Club club = new Club(naam, land, capaciteitStadion, oprichtingsdatum, trainer);

            List<ValidationResult> errors = new List<ValidationResult>();
            bool isValid = Validator.TryValidateObject(club, new ValidationContext(club), errors, true);

            if (isValid)
            {
                _repository.CreateClub(club);
            }

            return club;
        }


        public Competitie GetCompetition(int id)
        {
            return _repository.ReadCompetition(id);
        }

        public List<Competitie> GetAllCompetitions()
        {
            return _repository.ReadAllCompetitions();
        }

        public List<Competitie> GetAllCompetitionsWithClubs()
        {
            return _repository.ReadAllCompetitionsWithClubs();
        }

        public List<Competitie> GetCompetitionsByNameAndOprichtingsjaar(string name, int oprichtingsjaar)
        {
            return _repository.ReadCompetitionsByNameAndOprichtingsjaar(name, oprichtingsjaar);
        }

        public Competitie AddCompetition(string naam, string locatie, int oprichtingsjaar)
        {
            Competitie competitie =
                new Competitie(naam, locatie, oprichtingsjaar);

            List<ValidationResult> errors = new List<ValidationResult>();
            bool valid = Validator.TryValidateObject(competitie, new ValidationContext(competitie), errors, true);

            if (valid)
            {
                _repository.CreateCompetition(competitie);
            }

            return competitie;
        }


        public void AddClubCompetition(ClubCompetitie clubCompetitie)
        {
            _repository.CreateClubCompetition(clubCompetitie);
        }

        public void RemoveClubCompetition(int clubId, int competitieId)
        {
            _repository.DeleteClubCompetition(clubId, competitieId);
        }

        public Speler GetPlayer(int id)
        {
            return _repository.ReadPlayer(id);
        }


        public List<Speler> GetAllPlayers()
        {
            return _repository.ReadAllPlayers();
        }

        public List<Speler> GetAllPlayersWithClub()
        {
            return _repository.ReadAllPlayersWithClub();
        }
    }
}