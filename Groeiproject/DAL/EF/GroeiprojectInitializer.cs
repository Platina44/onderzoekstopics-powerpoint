﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SC.BL.Domain;

namespace SC.DAL.EF
{
    public static class GroeiprojectInitializer
    {
        private static bool _hasRunDuringAppExecution;

        public static void Initialize(GroeiprojectDbContext dbContext, bool dropCreateDatabase = false)
        {
            if (!_hasRunDuringAppExecution)
            {
                if (dropCreateDatabase)
                {
                    dbContext.Database.EnsureDeleted();
                }

                if (dbContext.Database.EnsureCreated())
                {
                    //Seed(dbContext);
                    _hasRunDuringAppExecution = true;
                }
            }
        }

        private static void Seed(GroeiprojectDbContext dbContext)
        {
            Club club01 = new Club("KV Mechelen", "België", 18477, new DateTime(1904, 10, 1));
            Club club02 = new Club("Royal Antwerp FC", "België", 16649, new DateTime(1880, 9, 1));
            Club club03 = new Club("Liverpool", "Verenigd Koninkrijk", 54074, new DateTime(1892, 3, 15));
            Club club04 = new Club( "Borussia Dortmund", "Duitsland", 81365, new DateTime(1909, 12, 19));

            Competitie competitie01 = new Competitie(1, "Jupiler Pro League", "België", 1895);
            Competitie competitie02 = new Competitie(2, "Premier League", "Verenigd Koninkrijk", 1992);
            Competitie competitie03 = new Competitie(3, "Bundesliga", "Duitsland", 1963);
            Competitie competitie04 = new Competitie(4, "Champions League", "Europa", 1992);

            Speler speler01 = new Speler(1, "Aster Vranckx", 40, 1.83, new DateTime(2002, 10, 4),
                SpelerPostitie.Middenvelder);
            Speler speler02 = new Speler(2, "Gaëtan Coucke", 28, 1.87, new DateTime(1998, 11, 3),
                SpelerPostitie.Doelman);
            Speler speler03 = new Speler(3, "Dieumerci Mbokani", 70, 1.85, new DateTime(1985, 11, 22),
                SpelerPostitie.Aanvaller);
            Speler speler04 = new Speler(4, "Virgil van Dijk", 4, 1.93, new DateTime(1991, 7, 8),
                SpelerPostitie.Verdediger);
            Speler speler05 = new Speler(5, "Erling Braut Haaland", 9, 1.94, new DateTime(2000, 7, 21),
                SpelerPostitie.Aanvaller);

            dbContext.Spelers.Add(speler01);
            dbContext.Spelers.Add(speler02);
            dbContext.Spelers.Add(speler03);
            dbContext.Spelers.Add(speler04);
            dbContext.Spelers.Add(speler05);

            ClubSpeler cs01 = new ClubSpeler {Club = club01, Speler = speler01};
            ClubSpeler cs02 = new ClubSpeler {Club = club01, Speler = speler02};
            
            ClubSpeler cs03 = new ClubSpeler {Club = club02, Speler = speler03};
            
            ClubSpeler cs04 = new ClubSpeler {Club = club03, Speler = speler04};
            
            ClubSpeler cs05 = new ClubSpeler {Club = club04, Speler = speler05};

            club01.SpelersInClub = new List<ClubSpeler> {cs01, cs02};
            club02.SpelersInClub = new List<ClubSpeler> {cs03};
            club03.SpelersInClub = new List<ClubSpeler> {cs04};
            club04.SpelersInClub = new List<ClubSpeler> {cs05};

            dbContext.Clubs.Add(club01);
            dbContext.Clubs.Add(club02);
            dbContext.Clubs.Add(club03);
            dbContext.Clubs.Add(club04);

            ClubCompetitie cc01 = new ClubCompetitie {Club = club01, Competitie = competitie01};
            ClubCompetitie cc02 = new ClubCompetitie {Club = club01, Competitie = competitie04};

            ClubCompetitie cc03 = new ClubCompetitie {Club = club02, Competitie = competitie01};

            ClubCompetitie cc04 = new ClubCompetitie {Club = club03, Competitie = competitie02};
            ClubCompetitie cc05 = new ClubCompetitie {Club = club03, Competitie = competitie04};

            ClubCompetitie cc06 = new ClubCompetitie {Club = club04, Competitie = competitie03};
            ClubCompetitie cc07 = new ClubCompetitie {Club = club04, Competitie = competitie04};

            club01.ClubInCompetities = new List<ClubCompetitie> {cc01, cc02};
            club02.ClubInCompetities = new List<ClubCompetitie> {cc03};
            club03.ClubInCompetities = new List<ClubCompetitie> {cc04, cc05};
            club04.ClubInCompetities = new List<ClubCompetitie> {cc06, cc07};

            dbContext.Competities.Add(competitie01);
            dbContext.Competities.Add(competitie02);
            dbContext.Competities.Add(competitie03);
            dbContext.Competities.Add(competitie04);

            dbContext.SaveChanges();

            foreach (var entry in dbContext.ChangeTracker.Entries().ToList())
            {
                entry.State = EntityState.Detached;
            }
        }
    }
}