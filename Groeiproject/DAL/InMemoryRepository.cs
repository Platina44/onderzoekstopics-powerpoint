﻿using System;
using System.Collections.Generic;
using SC.BL.Domain;

namespace SC.DAL
{
    public class InMemoryRepository : IRepository
    {
        public static List<Speler> alleSpelers = new List<Speler>();
        public static List<Club> alleClubs = new List<Club>();
        public static List<Competitie> alleCompetities = new List<Competitie>();

        /*static void Seed()
        {
            Club club01 = new Club("KV Mechelen", "België", 18477, new DateTime(1904, 10, 1));
            Club club02 = new Club("Royal Antwerp FC", "België", 16649, new DateTime(1880, 9, 1));
            Club club03 = new Club( "Liverpool", "Verenigd Koninkrijk", 54074, new DateTime(1892, 3, 15));
            Club club04 = new Club("Borussia Dortmund", "Duitsland", 81365, new DateTime(1909, 12, 19));
            
            alleClubs.Add(club01);
            alleClubs.Add(club02);
            alleClubs.Add(club03);
            alleClubs.Add(club04);

            alleCompetities.Add(new Competitie(0, "Jupiler Pro League", "België", 1895));
            alleCompetities.Add(new Competitie(1, "Premier League", "Verenigd Koninkrijk", 1992));
            alleCompetities.Add(new Competitie(2, "Bundesliga", "Duitsland", 1963));
            alleCompetities.Add(new Competitie(3, "Champions League", "Europa", 1992));

            alleSpelers.Add(new Speler(0, "Aster Vranckx", 40, 1.83, new DateTime(2002, 10, 4),
                SpelerPostitie.Middenvelder));
            alleSpelers.Add(new Speler(1, "Gaëtan Coucke", 28, 1.87, new DateTime(1998, 11, 3),
                SpelerPostitie.Doelman));
            alleSpelers.Add(new Speler(2, "Dieumerci Mbokani", 70, 1.85, new DateTime(1985, 11, 22),
                SpelerPostitie.Aanvaller));
            alleSpelers.Add(new Speler(3, "Virgil van Dijk", 4, 1.93, new DateTime(1991, 7, 8),
                SpelerPostitie.Verdediger));
            alleSpelers.Add(new Speler(4, "Erling Braut Haaland", 9, 1.94, new DateTime(2000, 7, 21),
                SpelerPostitie.Aanvaller));
        }
        */

        public Club ReadClub(int id)
        {
            foreach (Club club in alleClubs)
            {
                if (club.Id.Equals(id))
                {
                    return club;
                }
            }

            return null;
        }

        public Club ReadClubWithCompetitionAndPlayers(int id)
        {
            throw new NotImplementedException();
        }

        public List<Club> ReadAllClubs()
        {
            return alleClubs;
        }

        public List<Club> ReadAllClubsWithCompetitions()
        {
            throw new NotImplementedException();
        }

        public List<Club> ReadClubsOfCountry(string country)
        {
            List<Club> list = new List<Club>();

            foreach (Club club in alleClubs)
            {
                if (club.Land.Equals(country))
                {
                    list.Add(club);
                }
            }

            return list;
        }

        public Competitie ReadClubsOfCompetition(int competitieId)
        {
            throw new NotImplementedException();
        }

        public void CreateClub(Club club)
        {
            alleClubs.Add(
                new Club(club.Naam, club.Land, club.CapaciteitStadion, club.Oprichtingsdatum));
        }


        public Competitie ReadCompetition(int id)
        {
            foreach (Competitie competitie in alleCompetities)
            {
                if (competitie.Id.Equals(id))
                {
                    return competitie;
                }
            }

            return null;
        }

        public List<Competitie> ReadAllCompetitions()
        {
            return alleCompetities;
        }

        public List<Competitie> ReadAllCompetitionsWithClubs()
        {
            throw new NotImplementedException();
        }

        public List<Competitie> ReadCompetitionsByNameAndOprichtingsjaar(string name, int oprichtingsjaar)
        {
            List<Competitie> list = new List<Competitie>();

            foreach (Competitie competitie in alleCompetities)
            {
                if (competitie.Naam.ToLower().Contains(name.ToLower()) && competitie.Oprichtingsjaar < oprichtingsjaar)
                {
                    list.Add(competitie);
                }
            }

            return list;
        }

        public void CreateCompetition(Competitie competitie)
        {
            alleCompetities.Add(new Competitie(alleCompetities.Count, competitie.Naam, competitie.Locatie,
                competitie.Oprichtingsjaar));
        }

        public void CreateClubCompetition(ClubCompetitie clubCompetitie)
        {
            throw new NotImplementedException();
        }

        public void DeleteClubCompetition(int clubId, int competitieId)
        {
            throw new NotImplementedException();
        }

        public Speler ReadPlayer(int id)
        {
            foreach (Speler speler in alleSpelers)
            {
                if (speler.Id.Equals(id))
                {
                    return speler;
                }
            }

            return null;
        }

        public List<Speler> ReadAllPlayers()
        {
            return alleSpelers;
        }

        public List<Speler> ReadAllPlayersWithClub()
        {
            throw new NotImplementedException();
        }
    }
}