﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SC.DAL.Migrations
{
    public partial class AddTrainer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Trainer",
                table: "Clubs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Trainer",
                table: "Clubs");
        }
    }
}
